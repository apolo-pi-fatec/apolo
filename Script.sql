use projIn;

insert into usuario (nome, senha, login) values (
			"Ronaldo Galv�o", "12345", "Ronaldo"),
			("Geysa Santos", "23451", "Geysa"),
			("Matheus Marciano", "34512", "Matheus"),
			("Nicolas Rodrigues", "45123","Nicolas"),
			("Mike Barcelos", "51234", "Mike");

insert into informacoes (apelido, aniversario, id_usuario) values (
			"Rohh","1998-02-06", 1),
			("Gey", "1995-06-23", 2),
			("Marciano", "1998-07-13", 3),
			("Nick", "1998-05-24", 4),
			("Mike", "1996-05-06", 5);
	
insert into alarme (identificacao, horario, frequencia, id_usuario) values (
			"Aula 1", "18:45:00", "3", 1),
			("Aula 2", "19:35:00", "2", 2),
			("Aula 3", "20:25:00", "5", 3),
			("Aula 4", "21:25:00", "5", 4),
			("Aula 5", "22:15:00", "2", 5);
		
insert into calendario (identificacao, descricao, dia, id_usuario, id_alarme) values (
			"Aula do Sabha", "Arquitetura e organizacao", "2020-05-14", 1, 1),
			("Aula do Nadalete", "Algoritmo","2020-05-12", 2, 2),
			("Aula do Goretti", "Comunicacao","2020-05-13", 3, 3),
			("Aula Geraldo", "Fundamentos da Administracao","2020-05-11", 4, 4),
			("Aula do Marcos", "Matematica discreta","2020-05-15", 5, 5);
		
insert into anotacoes (titulo, diretorio, id_usuario) values (
			"Tarefas", "Do Mike", 5),
			("Provas", "Da Geysa", 2),
			("Trabalhos", "Do Matheus", 3),
			("Atividades para nota", "Do Ronaldo", 1),
			("Projeto Integrador", "Do Nicolas", 4);
			