# Assistente Pessoal - Apolo

## Equipe

### Dev Team
Geysa Santos: https://www.linkedin.com/in/geysa-fernanda-f-f-santos-97159b10a/  
Mike Barcelos: https://www.linkedin.com/in/mike-barcelos-b4648016a/  
Ronaldo Galvão: https://www.linkedin.com/in/ronaldo-galv%C3%A3o-13903915a/  

### Masters
Mayara Ferreira: https://www.linkedin.com/in/mayaracsf/  
Willian Dener: https://www.linkedin.com/in/willian-dener  

## Quem é o Apolo?
Um assistente pessoal virtual para plataforma web, com funcionalidades voltadas a facilitação do dia a dia do usuário, voltado ao contexto estudantil.

## E por quê ele foi criado?
Com a atualidade demandando tempo e eficácia em todas as atividades, nada melhor do que ter uma mãozinha que te auxilie nas tarefas. Seja na organização, no lembrete de eventos importantes, ou em uma pesquisa rápida sobre um tema.
Apolo te auxilia em tudo isso e muito mais. Com reconhecimento de voz, com um comando você pode acessar todo conteúdo de uma matéria, pode agendar provas para que o programa te avise quando começar a estudar e inclusive pode calcular a quantidade de faltas restante no semestre sem qualquer dor de cabeça.

## Funcionalidades
As funcionalidades do sistema por ordem de prioridade:

* **Saldar o usuário:** Interação personalizada do Apolo com o usuário no momento do acesso;
* **Anotações por voz:** Onde o usuário poderá criar as anotações de aulas e matérias;
* **Pesquisa no Google:** Pesquisas no Google a partir de comando de voz;
* **Calculadora por voz:** Através de comandos é possível realizar calculos aritméticos;
* **Leitor de arquivos:** O usuário poderá inserir arquivos .txt e o Apolo realizará a leitura deste;
* **Abertura de Sites diversos:** Apolo abre outros sites a partir do comando "abrir + nome do site" em uma nova aba do navegador;
* **Sugestão de filmes (desenvolvido pelo 6º semestre):** irá trazer sugestões de filmes (preço, plataforma (site/link) com base em parâmetros de busca, Rank e gênero do filme), utiliza as bibliotecas da IMDB e JUSTWATCH;


## Ferramentas
Utilizamos as seguintes ferramentas:

* Linguagem principal: Python 3.6;
* IDE's: PyCharm e/ou Visual Studio Code;
* Front End Web: Javascript;
* Framework: Django;
* Conceitos do SCRUM - Norteador do Projeto;
* Principais bibliotecas:  
    **SpeechRecognition**
    **SpeechSynthesis**  
    **PyAudio**   
* Sistema de Gerenciamento de Banco de Dados : MySQL 8.0
* Serviço de Banco de Dados Relacional : AWS



## Pré Requisito

* Possuir dispositivos de áudio e voz instalados:  
        _Gravação:_ microfone;  
        _Reprodução:_ alto-falantes;  
* Possuir acesso a internet;
* Possuir acesso a um browser;


## Apresentação do Projeto Integrador

* Apresentação de 17 de Maio:  
**Link:** https://drive.google.com/open?id=1XUmmvTQremZeKxwcIXfI9nrAjH411pLf   

* Apresentação de 31 de Maio:  
**Link:** https://drive.google.com/file/d/1J34voiAQr2qVYvra0R06xVMGxKqafMnr/view?usp=sharing

* Apresentação de 14 de Junho:  
**Link:** https://drive.google.com/file/d/1Tz7eLBo6Xpt7CBYJPqY5kW236hsxHXfR/view?usp=sharing

* Apresentação de 28 de Junho:  
**Link:** https://drive.google.com/file/d/1kMshhuh2A5RR75OXmeLqrwNFPPTI6Zie/view?usp=sharing

* Apresentação de 12 de Julho:  
**Link:** https://drive.google.com/file/d/1bXxrd6QFr1TmMNYZ8lCThhPDqMViYP3t/view?usp=sharing
