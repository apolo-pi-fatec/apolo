from django.urls import path
from . import views
urlpatterns = [
    path('register/', views.signUp.as_view(), name='signup'),
    path('', views.menu, name='menuprincipal'),
    path('calculator/', views.calculadora, name='calculadora'),
    path('menu/', views.menu2, name='menuReload'),
    path('ajuda/', views.ajuda, name='ajuda'),
    path('sobre/', views.sobre, name='sobre'),
    path('saudar/', views.saudando, name="saudar"),
    path('saudação/', views.attSaudacao, name='editsaudar'),
    path('notes/', views.anotacao, name="anotacao"),
    path('notes/<str:titulo>', views.noteView, name="note-view"),
    path('edit/<str:titulo>', views.noteEdit, name="note-edit"),
    path('delete/<str:titulo>', views.noteDelete, name="note-delete"),
    path('newnote/', views.newNote, name="new-note"),
    path('leitor/', views.leitor, name='view-leitor')
]
