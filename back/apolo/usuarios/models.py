from django.db import models
from django.contrib.auth import get_user_model

class Usuario(models.Model):
    id = models.AutoField(primary_key=True)

    login = models.CharField(
        max_length=64,
        null=False,
        blank=False
    )

    senha = models.CharField(
        max_length=64,
        null=False,
        blank=False
    )
    
    nome = models.CharField(
        max_length=64,
        null=False,
        blank=False
    )
    
    dt_criacao = models.DateTimeField(auto_now_add=True)

class Saudacao(models.Model):
    
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(get_user_model(),on_delete=models.CASCADE, related_name='login_phrase')
    phrase = models.CharField(max_length=70)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Anotacao(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(get_user_model(),on_delete=models.CASCADE, related_name='anotacao_content')
    titulo = models.CharField(max_length = 100, unique=True)
    anotacao = models.TextField()

    data_criacao = models.DateTimeField(auto_now_add=True)
    data_edicao = models.DateTimeField(auto_now=True)
