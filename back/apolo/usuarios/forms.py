from django import forms
from .models import Saudacao
from .models import Anotacao

class saveSaudacao(forms.ModelForm):

    class Meta:
        model = Saudacao
        fields = ('phrase',)



class novaNota(forms.ModelForm):

    class Meta:
        model = Anotacao
        fields = ('titulo','anotacao')