const startBtn = document.querySelector('#start');
const output = document.querySelector('#output');
let content = "Bem vindo!";
let myWindow;
let myFacebook;

function start() {
  const recognition = new webkitSpeechRecognition();
  recognition.interimResults = true;
  recognition.lang = "pt-BR";
  recognition.continuous = true;
  recognition.start();
  // This event happens when you talk in the microphone
  recognition.onresult = function(event) {
    for (let i = event.resultIndex; i < event.results.length; i++) {
      if (event.results[i].isFinal) {
        // Here you can get the string of what you told
        content = event.results[i][0].transcript.trim();
        console.log(content)
        //output.textContent = content;
        userCommand();
      // recognition.stop();
      }
    }
  };
};

function showIcons() {
  document.getElementById('bemvindo').style.display = "initial";
  document.getElementById('calculator').style.display = "initial";
  document.getElementById('buscando').style.display = "initial";
  document.getElementById('anotacao').style.display = "initial";
  document.getElementById('leitor').style.display = "initial";
}

function dismissButton() {
  document.getElementById("aviso").style.display = "none";
  document.getElementById("start").style.display = "none";
  showIcons();
}

function voiceBot(phrase) { 
    const timer = setInterval(function() {
        let voices = speechSynthesis.getVoices();
        if (voices.length !== 0) {
            let msg = new SpeechSynthesisUtterance(phrase);
            msg.rate = 1.1;
            msg.lang = "pt-BR";
            msg.voice = voices[16];
            window.speechSynthesis.speak(msg);
            clearInterval(timer);
          }
    }, 200);
}

function getmovie(){
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", 'https://r4u.herokuapp.com/getFilme/1', false ); // false for synchronous request
  xmlHttp.send( null );
  console.log(xmlHttp.responseText);
  const json = xmlHttp.responseText;
  const obj = JSON.parse(json);
  return obj.filme;

}

function welcomeMsg() {
    if (bemvindo === '') {
      voiceBot(content)
    }
    else {
      voiceBot(bemvindo)
    }
}
function userCommand() {
  content = content.toLowerCase();
    if(content === 'ajuda'){
      window.location.href = "http://localhost:8000/ajuda";
    }
    else if (content.includes('abrir')) {
      var resultado = content.split('abrir ');
      myFacebook = window.open(`https://www.${resultado[1]}.com`, '_blank');
    }
    else if(content === 'calculadora'){
      window.location.href = "http://localhost:8000/calculator";
    }
    else if(content === 'sair'){
      window.location.href = "http://localhost:8000/accounts/login/";
    }
    else if(content.includes('buscar por')){
      var resultado = content.split('buscar por');
      myWindow = window.open('https://www.google.com.br/search?q=' + resultado[1], '_blank');
    }
    else if(content.includes('buscar artigos')){
      var resultado = content.split('buscar artigos ');
      myWindow = window.open(`https://scholar.google.com.br/scholar?hl=pt-BR&as_sdt=0%2C5&q=${resultado[1]}`, '_blank');
    }
    else if (content === 'sugestão de filme'){
      const filme = getmovie();
      voiceBot(filme);
      document.getElementById('filme').innerHTML = 'Filme sugerido: '+ filme
    }
    else if(content === 'fechar'){
      if (myWindow !== undefined && myFacebook !== undefined){
        myWindow.close();
        myFacebook.close();
      }
      else if (myWindow !== undefined){
        myWindow.close();
      }
      else if (myFacebook !== undefined){
        myFacebook.close();
      }
      
    }
    else if (content === 'menu') {
      window.location.href = "http://localhost:8000/menu";
    }
    else if (content === 'sobre') {
     window.location.href = "http://localhost:8000/sobre";
    }
    else if (content === 'saudação') {
        window.location.href = "http://localhost:8000";
    }
    else if (content === 'anotações'){
      window.location.href = "http://localhost:8000/notes";
    }
    else if (content === 'nova anotação') {
      window.location.href = "http://localhost:8000/newnotes";
    }
    else if (content === "leitor"){
      window.location.href = "http://localhost:8000/leitor";
    }
    else{
      const ajuda = 'Não entendi'
      voiceBot(ajuda)
    }
}