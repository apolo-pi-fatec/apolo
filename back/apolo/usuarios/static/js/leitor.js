let content = '';
let input = document.querySelector('input');
let textarea = document.querySelector('textarea');
 
input.addEventListener('change', () => {
    let files = input.files;
 
    if(files.length == 0) return;
 
    const file = files[0];
 
    let reader = new FileReader();
 
    reader.onload = (e) => {
        const file = e.target.result;
        const lines = file.split(/\r\n|\n/);
        textarea.value = lines.join('\n');
        voiceBot(lines);
    };
 
    reader.onerror = (e) => alert(e.target.error.name);
 
    reader.readAsText(file);
    
});

function start() {
    const recognition = new webkitSpeechRecognition();
    recognition.interimResults = true;
    recognition.lang = "pt-BR";
    recognition.continuous = true;
    recognition.start();
    // This event happens when you talk in the microphone
    recognition.onresult = function(event) {
      for (let i = event.resultIndex; i < event.results.length; i++) {
        if (event.results[i].isFinal) {
          // Here you can get the string of what you told
          content = event.results[i][0].transcript.trim();
          console.log(content)
          //output.textContent = content;
          userCommand();
        // recognition.stop();
        }
      }
    };
};

function voiceBot(phrase) { 
    const timer = setInterval(function() {
        let voices = speechSynthesis.getVoices();
        if (voices.length !== 0) {
            let msg = new SpeechSynthesisUtterance(phrase);
            msg.rate = 1.1;
            msg.lang = "pt-BR";
            msg.voice = voices[16];
            window.speechSynthesis.speak(msg);
            clearInterval(timer);
          }
    }, 200);
}

function userCommand() {
    content = content.toLowerCase();
    if (content.includes('título')){
        let resultado = content.split('título');
        document.getElementById('id_titulo').value = resultado[1];
    }
    else if (content === 'menu' || content == "voltar"){
        window.location.href = "http://localhost:8000/menu";
    }
    else if (content === 'sobre') {
        window.location.href = "http://localhost:8000/sobre";
    }
    else if (content === 'ajuda') {
        window.location.href = "http://localhost:8000/ajuda";
    }
    else if (content === 'leitor' || content === 'outro arquivo'){
        window.location.href = "http://localhost:8000/leitor";
    }
    else {
        const ajuda = 'Não entendi'
        voiceBot(ajuda)
    }
}