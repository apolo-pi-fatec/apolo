let content = '';
function start() {
    const recognition = new webkitSpeechRecognition();
    recognition.interimResults = true;
    recognition.lang = "pt-BR";
    recognition.continuous = true;
    recognition.start();
    // This event happens when you talk in the microphone
    recognition.onresult = function(event) {
      for (let i = event.resultIndex; i < event.results.length; i++) {
        if (event.results[i].isFinal) {
          // Here you can get the string of what you told
          content = event.results[i][0].transcript.trim();
          console.log(content)
          //output.textContent = content;
          userCommand();
        // recognition.stop();
        }
      }
    };
  };

  function voiceBot(phrase) { 
    const timer = setInterval(function() {
        let voices = speechSynthesis.getVoices();
        if (voices.length !== 0) {
            let msg = new SpeechSynthesisUtterance(phrase);
            msg.rate = 1.1;
            msg.lang = "pt-BR";
            msg.voice = voices[16];
            window.speechSynthesis.speak(msg);
            clearInterval(timer);
          }
    }, 200);
}

  function userCommand() {
    content = content.toLowerCase();
    if (content.includes('abrir anotação')){
      let resultado = content.split('abrir anotação ');
      window.location.href = "http://localhost:8000/notes/" + resultado[1];
    }
    else if (content.includes('deletar')){
      let resultado = content.split('deletar ');
      window.location.href = "http://localhost:8000/notes/delete/" + resultado[1];
    }
    else if (content === 'nova anotação'){
        window.location.href = "http://localhost:8000/newnote";
    }
    else if (content === 'menu'){
        window.location.href = "http://localhost:8000/menu";
    }
    else if (content === 'sobre') {
      window.location.href = "http://localhost:8000/sobre";
    }
    else if (content === 'ajuda') {
      window.location.href = "http://localhost:8000/ajuda";
    }
    else {
      const ajuda = 'Não entendi'
      voiceBot(ajuda)
    }
  }