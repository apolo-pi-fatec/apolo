let content = '';
function start() {
    const recognition = new webkitSpeechRecognition();
    recognition.interimResults = true;
    recognition.lang = "pt-BR";
    recognition.continuous = true;
    recognition.start();
    // This event happens when you talk in the microphone
    recognition.onresult = function(event) {
      for (let i = event.resultIndex; i < event.results.length; i++) {
        if (event.results[i].isFinal) {
          // Here you can get the string of what you told
          content = event.results[i][0].transcript.trim();
          console.log(content)
          //output.textContent = content;
          userCommand();
        // recognition.stop();
        }
      }
    };
};

function voiceBot(phrase) { 
    const timer = setInterval(function() {
        let voices = speechSynthesis.getVoices();
        if (voices.length !== 0) {
            let msg = new SpeechSynthesisUtterance(phrase);
            msg.rate = 1.1;
            msg.lang = "pt-BR";
            msg.voice = voices[16];
            window.speechSynthesis.speak(msg);
            clearInterval(timer);
          }
    }, 200);
}

function userCommand() {
    content = content.toLowerCase();
    if (content.includes('título')){
        let resultado = content.split('título');
        document.getElementById('id_titulo').value = resultado[1];
    }
    else if (content.includes('anotar')){
        let resultado = content.split('anotar');
        document.getElementById('id_anotacao').value = resultado[1];
    }
    else if (content === 'salvar'){
        document.getElementById('note-save').click()
    }
    else if (content === 'anotações' || content === 'voltar'){
        window.location.href = "http://localhost:8000/notes";
    }
    else if (content === 'menu'){
        window.location.href = "http://localhost:8000/menu";
    }
    else if (content === 'sobre') {
        window.location.href = "http://localhost:8000/sobre";
    }
      else if (content === 'ajuda') {
        window.location.href = "http://localhost:8000/ajuda";
    }
    else {
        const ajuda = 'Não entendi'
        voiceBot(ajuda)
    }
}