function getHistory(){
	return document.getElementById("history-value").innerText;
}
function printHistory(num){
	document.getElementById("history-value").innerText=num;
}
function getOutput(){
	return document.getElementById("output-value").innerText;
}
function printOutput(num){
	if(num==""){
		document.getElementById("output-value").innerText=num;
	}
	else{
		document.getElementById("output-value").innerText=getFormattedNumber(num);
	}	
}
function getFormattedNumber(num){
	if(num=="-"){
		return "";
	}
	var n = Number(num);
	var value = n.toLocaleString("en");
	return value;
}
function reverseNumberFormat(num){
	return Number(num.replace(/,/g,''));
}
var operator = document.getElementsByClassName("operator");
for(var i =0;i<operator.length;i++){
	operator[i].addEventListener('click',function(){
		if(this.id=="clear"){
			printHistory("");
			printOutput("");
		}
		else if(this.id=="backspace"){
			var output=reverseNumberFormat(getOutput()).toString();
			if(output){//if output has a value
				output= output.substr(0,output.length-1);
				printOutput(output);
			}
		}
		else{
			var output=getOutput();
			var history=getHistory();
			if(output==""&&history!=""){
				if(isNaN(history[history.length-1])){
					history= history.substr(0,history.length-1);
				}
			}
			if(output!="" || history!=""){
				output= output==""?output:reverseNumberFormat(output);
				history=history+output;
				if(this.id=="="){
					var result=eval(history);
					printOutput(result);
					printHistory("");
				}
				else{
					history=history+this.id;
					printHistory(history);
					printOutput("");
				}
			}
		}
		
	});
}
var number = document.getElementsByClassName("number");
for(var i =0;i<number.length;i++){
	number[i].addEventListener('click',function(){
		var output=reverseNumberFormat(getOutput());
		if(output!=NaN){ //if output is a number
			output=output+this.id;
			printOutput(output);
		}
	});
}
function evaluate(input){
	input = input.replace("x","*");
	console.log(input);
	try{
		var result = eval(input);
		document.getElementById("output-value").innerText = result;
	}
	catch(e){
		console.log(e);
		document.getElementById("output-value").innerText = "";
	}
}
var microphone = document.getElementById('microphone');
microphone.onclick=function(){
	microphone.classList.add("record");
	var recognition = new (window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition)();
	recognition.lang = 'pt-BR';
	recognition.start();
	operations = {"mais":"+",
				 "menos":"-",
				 "multiplica":"*",
				 "vezes":"*",
				 "dividido por":"/",
				 "porcento":"%"}
	recognition.onresult = function(event){
		var input = event.results[0][0].transcript;
		console.log(input);
		for(property in operations){
			input= input.replace(property, operations[property]);
		}
		document.getElementById("output-value").innerText = input;
		setTimeout(function(){
			evaluate(input);
		}, 2000);
		microphone.classList.remove("record");
	}
	
}

function startonLoad() {
	const recognition = new webkitSpeechRecognition();
	recognition.interimResults = true;
	recognition.lang = "pt-BR";
	recognition.continuous = true;
	recognition.start();
	// This event happens when you talk in the microphone
	recognition.onresult = function(event) {
	  for (let i = event.resultIndex; i < event.results.length; i++) {
		if (event.results[i].isFinal) {
		  // Here you can get the string of what you told
		  content = event.results[i][0].transcript.trim();
		  console.log(content);
		  if (content === "menu"){
			window.location.href = "http://localhost:8000/menu";
		  }
		  else {
			evaluate(content);
		  }
		  startonLoad();
		  //output.textContent = content;
		// recognition.stop();
		}
	  }
	};
  };
