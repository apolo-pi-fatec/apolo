from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from .forms import saveSaudacao, novaNota
from .models import Saudacao, Anotacao
import logging


class signUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/register.html'

@login_required
def menu(request):
    saudacao = Saudacao.objects.filter(user_id=request.user).first()
    return render(request, 'usuarios/menu.html', {'saudacao': saudacao})

@login_required
def calculadora(request):
    return render(request, 'usuarios/calc.html')

@login_required
def menu2(request):
        saudacao = Saudacao.objects.filter(user_id=request.user).first()
        return render(request, 'usuarios/menu2.html', {'saudacao': saudacao})

@login_required
def ajuda(request):
    return render(request, 'usuarios/ajuda.html')

@login_required
def sobre(request):
    return render(request, 'usuarios/sobre.html')

@login_required
def saudando(request):
    if request.method == 'POST':
        form = saveSaudacao(request.POST)

        if form.is_valid():
            saudar = form.save(commit=False)
            saudar.user_id = request.user
            saudar.save()
            return redirect('http://localhost:8000/menu/')
    else:
        form = saveSaudacao()
        return render(request, 'saudacao/saudacao.html', {'form': form})

@login_required
def attSaudacao(request):
    saudacao = Saudacao.objects.filter(user_id=request.user).first()
    form = saveSaudacao(instance=saudacao)
    if request.method == 'POST':
        form = saveSaudacao(request.POST, instance=saudacao)

        if (form.is_valid()):
            saudacao.save()
            return redirect('http://localhost:8000/menu/')
    else:
        return render(request, 'saudacao/editsaudacao.html', {'form': form})

@login_required
def anotacao(request):
    notes = Anotacao.objects.all().filter(user_id=request.user).order_by('-data_criacao')
    return render(request, 'anotacoes/anotacao.html', {'notes': notes})

@login_required
def noteView(request, titulo):
    notes = get_object_or_404(Anotacao, titulo=titulo)
    return render(request, 'anotacoes/openote.html', {'notes': notes})

@login_required
def newNote(request):
    if request.method == 'POST':
        form = novaNota(request.POST)
        
        if form.is_valid():
            note = form.save(commit=False)
            note.user_id = request.user
            note.save()
            return redirect('http://localhost:8000/notes/')
    else:
        form = novaNota()
        return render(request, 'usuarios/newNote.html', {'form': form})

@login_required
def noteEdit(request, titulo):
    notes = get_object_or_404(Anotacao, titulo=titulo)
    form = novaNota(instance=notes)
    if request.method == 'POST':
        form = novaNota(request.POST, instance=notes)

        if (form.is_valid()):
            notes.save()
            return redirect('http://localhost:8000/notes/')
    else:
        return render(request, 'usuarios/editnote.html', {'form': form})

@login_required
def noteDelete(request, titulo):
    notes = get_object_or_404(Anotacao, titulo=titulo)
    notes.delete()
    return redirect('http://localhost:8000/notes/')

@login_required
def leitor(request):
    return render(request, 'usuarios/leitor.html')