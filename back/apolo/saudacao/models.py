from django.db import models


class Saudacaoo(models.Model):
    
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(Usuario,on_delete=models.CASCADE, related_name='phrase_login')
    phrase = models.CharField(max_length=70)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
