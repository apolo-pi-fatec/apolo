import speech_recognition as sr

def ouvir_microfone():

    microfone = sr.Recognizer()
    with sr.Microphone() as source:

        microfone.adjust_for_ambient_noise(source)
        print("Diga alguma coisa: ")
        audio = microfone.listen(source)

    try:

        frase = microfone.recognize_google(audio, language='pt-BR')
        print("Você disse: " + frase)
        with open('arq01.txt', 'a') as arquivo:
            arquivo.write(frase + "\n")

    except sr.UnkownValueError:
        print("Não entendi")

    return frase

frase = ouvir_microfone()