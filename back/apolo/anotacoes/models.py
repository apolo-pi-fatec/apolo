from django.db import models

class Anotacaoo(models.Model):
    titulo = models.CharField(max_length = 100, unique=True)
    anotacao = models.TextField()

    data_criacao = models.DateTimeField(auto_now_add=True)
    data_edicao = models.DateTimeField(auto_now=True)