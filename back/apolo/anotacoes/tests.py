from models import Anotacoes

anotacao_criar = Anotacoes(titulo="meu titulo", anotacao="minha nova anotacao")
anotacao_criar.save()

anotacao_ler = Anotacoes()
anotacao_ler.filter(titulo="meu titulo")

anotacao_deletar = Anotacoes()
anotacao_deletar.filter(titulo="meu titulo").delete()

anotacao_editar = Anotacoes()
anotacao_editar.filter(titulo="meu titulo").update(
    titulo="meu novo titulo atualizado",
    anotacao="minha nova anotacao atualizada")