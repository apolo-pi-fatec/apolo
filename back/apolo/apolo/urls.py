from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('usuarios.urls')),
    path('accounts/', include('usuarios.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('calculator/', include('usuarios.urls')),
    path('menu/', include('usuarios.urls')),
    path('ajuda/', include('usuarios.urls')),
    path('sobre/', include('usuarios.urls')),
    path('saudar/', include('usuarios.urls')),
    path('saudação/', include('usuarios.urls')),
    path('notes/', include('usuarios.urls')),

]
